#!/bin/bash

#stop if OS is not redhat
if [ ! -f /etc/redhat-release ]; then
    echo 'File /etc/redhat-release not found';
    return;
fi

#get version OS
version=$(rpm -q --qf "%{VERSION}" $(rpm -q --whatprovides redhat-release));

#install package
rpm -ivh http://repo.zabbix.com/zabbix/3.0/rhel/$version/x86_64/zabbix-release-3.0-1.el$version.noarch.rpm;

#no comments
yum install zabbix-agent;

# add to autoload
chkconfig zabbix-agent on;

#backup config
cp /etc/zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.default;

#Change Hostname in config file
echo "Set Hostname (Zabbix Server):"
read hostname;
if [[ ! -z $hostname ]]; then
   sed -i "s/^Hostname=.*$/Hostname=$hostname/" /etc/zabbix/zabbix_agentd.conf
fi

#Change Server in config file
echo "Set Server (127.0.0.1):"
read server;
if [[ ! -z $server ]]; then
   sed -i "s/^Server=.*$/Server=$server/" /etc/zabbix/zabbix_agentd.conf
fi

#Start agent
service zabbix-agent start

#check
ps aux | grep zabbix
netstat -anp | grep 10050